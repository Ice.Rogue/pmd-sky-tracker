# PMD Sky Tracker


## Name
Pokémon Mystery Dungeon: Explorers of Sky Tracker

## Description
This is a tracker that was originally requested by ChaosTales (https://www.twitch.tv/chaostales) to track their nuzlocke runs of Pokémon Mystery Dungeon: Explorers of Sky. 
It can be used for tracking other styles of playthroughs as well, though there will be issues if you catch more than one of the same pkmn on the same floor of a dungeon.


## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
You will need to be running the latest version of Bizhawk (https://tasvideos.org/BizHawk) . Then download and place the folder with the PmdSky-Tracker files in the Lua folder within the Bizhawk root folder. 
Then once you run the Emuhawk.exe, open the Lua console and select the pmdsky-tracker.lua file. 
You are ready to load your PMD: Sky game if it is not already loaded.

## Usage
Shows active team as well as the recruited team members. Several areas of the tracker will provide more information (like stats on the members) when you mouse over them.


## Contributing
Open to any ideas, suggestions, or contributions. 

## Authors and acknowledgment
Big contributions came from ChaosTales (massive knowlege of PMD) as well as UTDZac (help with understanding Lua and some chats in discord).
Parts of code were taken or inspired by (not sure how much remain at this point) from the IronMon Trackers for Generation 3 (https://github.com/besteon/Ironmon-Tracker) and NDS (https://github.com/Brian0255/NDS-Ironmon-Tracker)

