Main = {
	team = {},
	activeTeam = {},
	fullTeam = {},
	teammembers = 0,
	activeHoverFrame = nil,
	belly = 0,
	max_belly = 0,

	posXBase = 230,
	posYBase = 55,
	spaceX = 55,
	spaceY = 45,
	numPerLine = 4,
	formID = 0,
	NaEu = "",
	refreshFramesHover = 30,
	refreshFramesData = 45,
	refreshFramesTeam = 600,
	hashNA = "10AF6A1A4D90FFB48BFB6C444324F7FD",
	hashEU = "6735749E060E002EFD88E61560E45567",
	pmdSkyGameCode = 4286627304,
	pmdSkyVameVersion = 33576673,
}

function Main.Initialize()
	console.clear()                 -- Clearing the console for each new game helps with troubleshooting issues
	Main.DataFolder = "pmdsky_tracker" -- Root folder for the project data and sub scripts
	Main.TrackerFiles = {
		"/ASCII.lua",
		"/AdventureLog.lua",
		"/Constants.lua",
		"/Drawing.lua",
		"/Dungeons.lua",
		"/DungeonData.lua",
		"/Input.lua",
		"/Items.lua",
		"/MonsterID.lua",
		"/Moves.lua",
		"/Overlays.lua",
		"/Program.lua",
		"/Status.lua",
		"/Tactic.lua",
		"/Team.lua",
		"/TeamMemberData.lua",
	}

	for _, file in ipairs(Main.TrackerFiles) do
		local path = Main.DataFolder .. file
		if Main.FileExists(path) then
			dofile(path)
		else
			print("Unable to load " .. path ..
				"\nMake sure all of the downloaded Tracker's files are still together.")
			Main.DisplayError("Unable to load " .. path ..
				"\n\nMake sure all of the downloaded Tracker's files are still together.")
			return false
		end
	end
	return true
end

-- Checks if a file exists
function Main.FileExists(path)
	local file = io.open(path, "r")
	if file ~= nil then
		io.close(file)
		return true
	else
		return false
	end
end

-- Main loop
function Main.Run()
	if gameinfo.getromname() == "Null" then
		print("Waiting for a game ROM to be loaded... (File -> Open ROM)")
	end

	local romLoaded = false
	while not romLoaded do
		if gameinfo.getromname() ~= "Null" then romLoaded = true end
		emu.frameadvance()
	end
	local gamecode = memory.read_u32_be(0x0000AC)
	local gameversion = memory.read_u32_be(0x0000BC)
	local gamehash = gameinfo.getromhash()
	local romname = gameinfo.getromname()
	-- getromname: Pokemon Mystery Dungeon - Explorers of Sky (USA)
	-- gamecode: 4286627304
	-- gameversion: 33576673

	print("ROM Name: " .. romname)
	print("Game Code: " .. gamecode)
	print("Game Version: " .. gameversion)
	print("Game Hash: " .. gamehash)
	if gamecode ~= Main.pmdSkyGameCode and gameversion ~= Main.pmdSkyGameVersion then
		-- If the loaded game is unsupported, remove the Tracker padding but continue to let the game play.
		client.SetGameExtraPadding(0, 0, 0, 0)
		print("Unsupported Game Detected")
		while true do
			emu.frameadvance()
		end
	else
		client.SetGameExtraPadding(0, 0, 250, 0)
		gui.defaultTextBackground(0)
		local infoBoxHeight = 200

		if gamehash == Main.hashEU then
			print("European Rom Detected")
			Main.NaEu = "EU"
		elseif gamehash == Main.hashNA then
			print("NA Rom Detected")
			Main.NaEu = "NA"
		else
			print("Unknown Rom Detected, using NA Addresses")
			Main.NaEu = "NA"
		end
		local topboxX = Constants.SCREEN.WIDTH + Constants.SCREEN.MARGIN

		-- event.onexit(Program.HandleExit, "HandleExit")

		local shift = 0xFFFFFF
		local carriedAddr = 0x22A4BB8
		local dungeon
		while true do
			-- void gui.drawRectangle(int x, int y, int width, int height, [luacolor line = nil], [luacolor background = nil], [string surfacename = nil])
			if emu.framecount() % Main.refreshFramesHover == 0 then
				gui.drawRectangle(265, 5, 230, 350,
					Constants.Color.DkIceBlue,
					Constants.Color.NeroGrey)

				Drawing.DrawHeader()
				Drawing.DrawTeam()
				Program.CheckForHover()
			end
			if emu.framecount() % Main.refreshFramesData == 0 then
				Program.GetData()
			end
			emu.frameadvance()
		end
	end
end

if Main.Initialize() then
	Main.Run()
end
