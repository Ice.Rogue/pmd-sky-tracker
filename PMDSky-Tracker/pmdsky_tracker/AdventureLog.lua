﻿AdventureLog = {
	logPtr = 0x20AFF78,
	logPtrNA = 0x20AFF78,
	logPtrEU = 0x20B0894,
	advLogAddr = 0,
}

function AdventureLog.Initialize()
	if Main.NaEu == "EU" then
		AdventureLog.logPtr = AdventureLog.logPtrEU
	end
	AdventureLog.advLogAddr = memory.read_u32_le(AdventureLog.logPtr)
end

function AdventureLog.GetAdventureLog()
	AdventureLog.Initialize()
	local dungeonClears = memory.read_u32_le(AdventureLog.advLogAddr + 0x10)
	local evolutions = memory.read_u32_le(AdventureLog.advLogAddr + 0x14)
	local eggsHatched = memory.read_u32_le(AdventureLog.advLogAddr + 0x1C)
	local faints = memory.read_u32_le(AdventureLog.advLogAddr + 0x24)
	local victoriesOnOneFloor = memory.read_u32_le(AdventureLog.advLogAddr + 0x28)
	local pokemonJoinedCounter = memory.read_u32_le(AdventureLog.advLogAddr + 0x2C)
	local pokemonBattledCounter = memory.read_u32_le(AdventureLog.advLogAddr + 0x30)
	local movesLearnedCounter = memory.read_u32_le(AdventureLog.advLogAddr + 0x34)
	local bigTreasureWins = memory.read_u32_le(AdventureLog.advLogAddr + 0x38)
	local recycled = memory.read_u32_le(AdventureLog.advLogAddr + 0x3C)
	--uint32_t pokemon_joined_flags[37];       // 0x44
	--uint32_t pokemon_battled_flags[37];      // 0xD8
	--uint32_t moves_learned_flags[17];        // 0x16C
	--uint32_t items_acquired_flags[44];       // 0x1B0
	--uint32_t special_challenge_flags;        // 0x260
	--uint32_t sentry_duty_game_points[5];     // 0x264
	return {
		dungeonClears = dungeonClears,
		evolutions = evolutions,
		eggsHatched = eggsHatched,
		faints = faints,
		victoriesOnOneFloor = victoriesOnOneFloor,
		pokemonJoinedCounter = pokemonJoinedCounter,
		pokemonBattledCounter = pokemonBattledCounter,
		movesLearnedCounter = movesLearnedCounter,
		bigTreasureWins = bigTreasureWins,
		recycled = recycled,
	}
end

function AdventureLog.DrawAdventureLog()
	local advLog = AdventureLog.GetAdventureLog()
	local rsp = 15
	local csp3 = 75
	local csp2 = 110
	local xb = 270
	local yb = 250

	-- void gui.drawRectangle(int x, int y, int width, int height, [luacolor line = nil], [luacolor background = nil], [string surfacename = nil])
	gui.drawRectangle(265, 245, 230, 110, Constants.Color.IceBlue, Constants.Color.DkNeroGrey)

	Drawing.drawStatText(xb, yb, "Adventure Log", Constants.Color.IceBlue,
		Constants.Color.Black, "bold")
	---
	---
	Drawing.drawSplitStatText(xb, yb + rsp, "Pkmn Fainted: ", advLog.faints, Constants.Color.LtBlue,
		Constants.Color.White, Constants.Color.DkGrey)

	Drawing.drawSplitStatText(xb + csp2, yb + rsp, "Pkmn Joined: ", advLog.pokemonJoinedCounter, Constants.Color.LtBlue,
		Constants.Color.White, Constants.Color.DkGrey)
	---
	---
	Drawing.drawSplitStatText(xb, yb + rsp * 2, "Times Recycled: ", advLog.recycled, Constants.Color.LtBlue,
		Constants.Color.White, Constants.Color.DkGrey)
	Drawing.drawSplitStatText(xb + csp2, yb + rsp * 2, "Types Battled: ", advLog.pokemonBattledCounter,
		Constants.Color.LtBlue, Constants.Color.White, Constants.Color.DkGrey)
	---
	---
	Drawing.drawSplitStatText(xb, yb + rsp * 3, "Dungeons Full Cleared: ", advLog.dungeonClears, Constants.Color.LtBlue,
		Constants.Color.White, Constants.Color.DkGrey)
	---
	---
	Drawing.drawSplitStatText(xb, yb + rsp * 4, "Most Pkmn Fained on a Floor: ", advLog.victoriesOnOneFloor,
		Constants.Color.LtBlue, Constants.Color.White, Constants.Color.DkGrey)
	---
	---
	Drawing.drawSplitStatText(xb, yb + rsp * 5, "Moves Learned: ", advLog.movesLearnedCounter,
		Constants.Color.LtBlue, Constants.Color.White, Constants.Color.DkGrey)
	Drawing.drawSplitStatText(xb + csp2, yb + rsp * 5, "Evolutions: ", advLog.evolutions,
		Constants.Color.LtBlue, Constants.Color.White, Constants.Color.DkGrey)
	---
	---
	Drawing.drawSplitStatText(xb, yb + rsp * 6, "Eggs Hatched: ", advLog.eggsHatched,
		Constants.Color.LtBlue, Constants.Color.White, Constants.Color.DkGrey)
	Drawing.drawSplitStatText(xb + csp2, yb + rsp * 6, "Big Treasure Wins: ", advLog.bigTreasureWins,
		Constants.Color.LtBlue, Constants.Color.White, Constants.Color.DkGrey)
	---
	---
end
