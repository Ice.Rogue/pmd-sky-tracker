﻿Team = {
	teamTableAddr = 0x22ABDE0,
	teamTableAddrNA = 0x22ABDE0,
	teamTableAddrEU = 0x22AC720,
}

function Team.Initialize()
	if Main.NaEu == "EU" then
		Team.teamTableAddr = Team.teamTableAddrEU
	end
end

function Team.PopulateTeamList()
	Team.Initialize()
	local tempTeam = {}
	local invalidCount = 0
	-- struct ground_monster members[555]
	-- ASSERT_SIZE(struct ground_monster, 68);
	for i = 0, 37672, 68 do
		local newmon = Team.GetTeamMembers(Team.teamTableAddr + i, i / 68)
		local monID = Program.toID(newmon.monster_id, newmon.joined_atID, newmon.joined_at_floor)
		if newmon.valid >= 1 then -- or (newmon.level > 0 and newmon.monster_id > 0 and newmon.max_hp > 0)
			if Main.fullTeam[monID] then
				if Main.fullTeam[monID].exp < newmon.exp then
					Main.fullTeam[monID].level = newmon.level
					Main.fullTeam[monID].max_hp = newmon.max_hp
					Main.fullTeam[monID].iq = newmon.iq
					Main.fullTeam[monID].exp = newmon.exp
					Main.fullTeam[monID].atk = newmon.atk
					Main.fullTeam[monID].spatk = newmon.spatk
					Main.fullTeam[monID].def = newmon.def
					Main.fullTeam[monID].spdef = newmon.spdef
					Main.fullTeam[monID].tactic = newmon.tactic
					Main.fullTeam[monID].tacticID = newmon.tacticID
					Main.fullTeam[monID].move1 = newmon.move1
					Main.fullTeam[monID].move1ID = newmon.move1ID
					Main.fullTeam[monID].move2 = newmon.move2
					Main.fullTeam[monID].move2ID = newmon.move2ID
					Main.fullTeam[monID].move3 = newmon.move3
					Main.fullTeam[monID].move3ID = newmon.move3ID
					Main.fullTeam[monID].move4 = newmon.move4
					Main.fullTeam[monID].move4ID = newmon.move4ID
					Main.fullTeam[monID].name = newmon.name
				end
			else
				Main.fullTeam[monID] = newmon
			end
			tempTeam[monID] = newmon
		else
			invalidCount = invalidCount + 1
		end
		if invalidCount >= 5 then
			break
		end
	end

	for k, v in pairs(Main.fullTeam) do
		if tempTeam[k] == nil then
			Main.fullTeam[k] = nil
		end
	end
end

function Team.GetTeamMembers(currAddr, i)
	local valid = memory.read_u8(currAddr)
	local monster_id = memory.read_u16_le(currAddr + 4)
	local level = memory.read_u8(currAddr + 1)
	local max_hp = memory.read_u16_le(currAddr + 10)
	if valid >= 1 then --or (monster_id > 0 and level > 0 and max_hp > 0)
		-- table.insert(Main.team,Team.Monster)
		-- local m = Main.team[#Main.team]
		--print(valid .. " - " .. currAddr)
		local monster_id = memory.read_u16_le(currAddr + 4)
		if monster_id > 600 then monster_id = monster_id - 600 end

		-- posX = posX + spaceX
		local monsterName = Monster_ID.MonsterID_MASTER_LIST[monster_id].name
		local monsterIcon = Monster_ID.MonsterID_IMG_LIST[string.lower(
			monsterName)]

		-- local m = Team.Monster
		local level = memory.read_u8(currAddr + 1)
		local joined_atID = memory.read_u8(currAddr + 2)
		if Dungeons.MASTER_LIST[joined_atID] ~= nil then
			joined_at = Dungeons.MASTER_LIST[joined_atID].name
		else
			joined_at = " "
		end
		local joined_at_floor = memory.read_u8(currAddr + 3)
		local iq = memory.read_u16_le(currAddr + 8)
		local exp = memory.read_s32_le(currAddr + 0x10)
		local max_hp = memory.read_u16_le(currAddr + 10)

		local atk = memory.read_u8(currAddr + 12)
		local spatk = memory.read_u8(currAddr + 13)
		local def = memory.read_u8(currAddr + 14)
		local spdef = memory.read_u8(currAddr + 15)

		local tactic = " "
		local tacticID = memory.read_u8(currAddr + 32)
		if Tactic.MASTER_LIST[tacticID] ~= nil then
			tactic = Tactic.MASTER_LIST[tacticID].name
		else
			tactic = " "
		end

		local moves = Moves.GetGroundMoves(currAddr + 34)

		local move1 = moves.move1
		local move1ID = moves.move1ID

		local move2 = moves.move2
		local move2ID = moves.move2ID

		local move3 = moves.move3
		local move3ID = moves.move3ID


		local move4 = moves.move4
		local move4ID = moves.move4ID

		-- local monsterName = " "
		-- local monsterNameBytes = memory.read_bytes_as_array(currAddr + 58, 10)
		-- for i, v in ipairs(monsterNameBytes) do
		-- print(teamNameBytes[i])
		--	monsterName = monsterName .. string.char(monsterNameBytes[i])
		-- end
		local name = Program.ReadName(currAddr + 58) .. " "

		local posX = Program.GetPosionOfImage(i).Xpos
		local posY = Program.GetPosionOfImage(i).Ypos
		local statuses = {}

		return {
			valid = valid,
			monster_id = monster_id,
			monsterName = monsterName,
			monsterIcon = monsterIcon,
			level = level,
			joined_atID = joined_atID,
			joined_at = joined_at,
			joined_at_floor = joined_at_floor,
			iq = iq,
			exp = exp,
			hp = 0,
			max_hp = max_hp,
			atk = atk,
			spatk = spatk,
			def = def,
			spdef = spdef,
			tactic = tactic,
			tacticID = tacticID,
			move1 = move1,
			move1ID = move1ID,
			move2 = move2,
			move2ID = move2ID,
			move3 = move3,
			move3ID = move3ID,
			move4 = move4,
			move4ID = move4ID,
			name = name,
			posX = posX,
			posY = posY,
			statuses = statuses,
			held_item = " ",
			is_team_leader = 0,
			team_index = 0,
			member_index = 0,
		}
	end
	return {
		valid = valid,
		monster_id = monster_id,
		level = level,
		max_hp = max_hp
	}
end
