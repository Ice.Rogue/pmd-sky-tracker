﻿DungeonData =
{
	dungeonTableAddrNA = 0x21B9D34,
	dungeonTableAddrEU = 0x21BA674,
	dungeonTableAddr = 0x21B9D34,
	--struct monster monsters[20]; // 0x7F4: Info for all the monsters currently in the dungeon
	monstersDungeonOffset = 0x7F4,
	tableAddr = 0x21B9D34 + 0x7F4,
}
function DungeonData.Initialize()
	if Main.NaEu == "EU" then
		DungeonData.dungeonTableAddr = DungeonData.dungeonTableAddrEU
	end
	DungeonData.tableAddr = DungeonData.dungeonTableAddr + DungeonData.monstersDungeonOffset
end

function DungeonData.GetCurrentDungeon()
	DungeonData.Initialize()
	local dungeon_id = memory.read_u8(DungeonData.dungeonTableAddr + 0x748)
	local currentFloor = memory.read_u8(DungeonData.dungeonTableAddr + 0x749)
	local dungeonName = " "
	if Dungeons.MASTER_LIST[dungeon_id] ~= nil then
		dungeonName = Dungeons.MASTER_LIST[dungeon_id].name
	else
		dungeonName = " "
	end
	return {
		dungeon_id = dungeon_id,
		currentFloor = currentFloor,
		dungeonName = dungeonName,
	}
end

function DungeonData.GetBelly()
	local bellyT = 0
	local belly_thousandths = 0
	local max_bellyT = 0
	local max_belly_thousandths = 0
	-- struct monster monsters[20]
	-- ASSERT_SIZE(struct monster, 576);
	DungeonData.Initialize()
	local dungeon = DungeonData.GetCurrentDungeon()
	local overlayCheck = Overlays.DungeonOverlayLoaded()
	--console.log(dungeon)
	if dungeon.dungeonName ~= "Dummy" and dungeon.dungeon_id > 0 and overlayCheck > 0 then
		for i = 0, 10944, 576 do
			local currectTableAddr = DungeonData.tableAddr + i
			local is_team_leader = memory.read_u8(currectTableAddr + 7)
			if (is_team_leader >= 1) then
				bellyT = memory.read_s16_le(currectTableAddr + 0x146)
				belly_thousandths = memory.read_s16_le(currectTableAddr + 0x148)
				max_bellyT = memory.read_s16_le(currectTableAddr + 0x14A)
				max_belly_thousandths = memory.read_s16_le(currectTableAddr + 0x14C)

				break
			end
		end
		if max_bellyT ~= nil and bellyT ~= nil and bellyT > 0 and max_bellyT > 0 and
			max_bellyT < 200 and bellyT < 200 then
			return { max_belly = max_bellyT, belly = bellyT }
		else
			return { max_belly = Main.max_belly, belly = Main.belly }
		end
	else
		return { max_belly = 0, belly = 0 }
	end
end

function DungeonData.GetTeamData()
	DungeonData.Initialize()
	local tempActiveTeam = {}
	local validMembers = 0
	for i = 0, 10944, 576 do
		local currAddr = DungeonData.tableAddr + i
		local newmon = {}
		newmon = DungeonData.GetMonsterData(currAddr)
		if newmon.valid > 0 then
			validMembers = validMembers + 1
			local monID = Program.toID(newmon.monster_id, newmon.joined_atID, newmon.joined_at_floor)
			if Main.fullTeam[monID] then
				if Main.fullTeam[monID].exp <= newmon.exp then
					Main.fullTeam[monID].level = newmon.level
					Main.fullTeam[monID].max_hp = newmon.max_hp + newmon.max_hp_boost
					Main.fullTeam[monID].iq = newmon.iq
					Main.fullTeam[monID].exp = newmon.exp
					Main.fullTeam[monID].atk = newmon.atk
					Main.fullTeam[monID].spatk = newmon.spatk
					Main.fullTeam[monID].def = newmon.def
					Main.fullTeam[monID].spdef = newmon.spdef
					Main.fullTeam[monID].tactic = newmon.tactic
					Main.fullTeam[monID].tacticID = newmon.tacticID
					Main.fullTeam[monID].move1 = newmon.move1
					Main.fullTeam[monID].move1ID = newmon.move1ID
					Main.fullTeam[monID].move2 = newmon.move2
					Main.fullTeam[monID].move2ID = newmon.move2ID
					Main.fullTeam[monID].move3 = newmon.move3
					Main.fullTeam[monID].move3ID = newmon.move3ID
					Main.fullTeam[monID].move4 = newmon.move4
					Main.fullTeam[monID].move4ID = newmon.move4ID
					Main.fullTeam[monID].member_index = newmon.member_index
					Main.fullTeam[monID].team_index = newmon.team_index
					Main.fullTeam[monID].is_team_leader = newmon.is_team_leader
					Main.fullTeam[monID].held_item = newmon.held_item
				end
			end
			if Main.activeTeam[monID] then
				if Main.activeTeam[monID].exp <= newmon.exp then
					Main.activeTeam[monID].level = newmon.level
					Main.activeTeam[monID].hp = newmon.hp
					Main.activeTeam[monID].max_hp = newmon.max_hp + newmon.max_hp_boost
					Main.activeTeam[monID].iq = newmon.iq
					Main.activeTeam[monID].exp = newmon.exp
					Main.activeTeam[monID].atk = newmon.atk
					Main.activeTeam[monID].spatk = newmon.spatk
					Main.activeTeam[monID].def = newmon.def
					Main.activeTeam[monID].spdef = newmon.spdef
					Main.activeTeam[monID].tactic = newmon.tactic
					Main.activeTeam[monID].tacticID = newmon.tacticID
					Main.activeTeam[monID].move1 = newmon.move1
					Main.activeTeam[monID].move1ID = newmon.move1ID
					Main.activeTeam[monID].move2 = newmon.move2
					Main.activeTeam[monID].move2ID = newmon.move2ID
					Main.activeTeam[monID].move3 = newmon.move3
					Main.activeTeam[monID].move3ID = newmon.move3ID
					Main.activeTeam[monID].move4 = newmon.move4
					Main.activeTeam[monID].move4ID = newmon.move4ID
					Main.activeTeam[monID].member_index = newmon.member_index
					Main.activeTeam[monID].team_index = newmon.team_index
					Main.activeTeam[monID].is_team_leader = newmon.is_team_leader
					Main.activeTeam[monID].held_item = newmon.held_item
					if Main.fullTeam[monID] then
						Main.activeTeam[monID].name = Main.fullTeam[monID].name
					end
				end
			else
				Main.activeTeam[monID] = newmon
			end
			tempActiveTeam[monID] = newmon
			--console.log(newmon)
		end
		if validMembers >= 4 then
			break
		end
	end
	for k, v in pairs(Main.activeTeam) do
		if tempActiveTeam[k] == nil then
			Main.activeTeam[k] = nil
		end
	end
end

function DungeonData.GetMonsterData(currAddr)
	local max_hp = memory.read_u16_le(currAddr + 0x12)
	local is_team_leader = memory.read_u8(currAddr + 7)
	local is_not_team_member = memory.read_u8(currAddr + 0x6)
	local atk = memory.read_u8(currAddr + 0x1A)
	local spatk = memory.read_u8(currAddr + 0x1B)
	local def = memory.read_u8(currAddr + 0x1C)
	local spdef = memory.read_u8(currAddr + 0x1D)
	local monster_id = memory.read_u16_le(currAddr + 2)
	if (is_not_team_member == 0) and (max_hp > 0 and atk > 0 and spatk > 0 and def > 0 and spdef > 0 and monster_id > 0) then
		local iq = memory.read_u16_le(currAddr + 0xE)
		local level = memory.read_u8(currAddr + 0xA)
		local max_hp_boost = memory.read_u16_le(currAddr + 0x16)
		local hp = memory.read_u16_le(currAddr + 0x10)
		local team_index = memory.read_u16_le(currAddr + 0xC)
		local joined_atID = memory.read_u8(currAddr + 0x48)
		local joined_at_floor = memory.read_u8(currAddr + 0x49)
		local exp = memory.read_s32_le(currAddr + 0x20)
		local tacticID = memory.read_u8(currAddr + 0xA8)

		local moves = Moves.GetDungeonMoves(currAddr + 0x124)

		local move1 = moves.move1
		local move1ID = moves.move1ID

		local move2 = moves.move2
		local move2ID = moves.move2ID

		local move3 = moves.move3
		local move3ID = moves.move3ID


		local move4 = moves.move4
		local move4ID = moves.move4ID

		--struct statuses statuses;  // 0xA9
		local statuses = {}
		--statuses = DungeonData.GetMonsterStatuses(currAddr + 0xA9)

		--	 // 0x218: Status icons displayed on top of the monster's sprite
		--	 struct status_icon_flags status_icons;
		statuses = DungeonData.GetMonsterStatusIcons(currAddr + 0x218)

		if monster_id > 600 then monster_id = monster_id - 600 end
		local monsterName = " "
		if Monster_ID.MonsterID_MASTER_LIST[monster_id] ~= nil then
			monsterName = Monster_ID.MonsterID_MASTER_LIST[monster_id].name
		end
		local monsterIcon = Monster_ID.MonsterID_IMG_LIST[string.lower(monsterName)]

		if Dungeons.MASTER_LIST[joined_atID] ~= nil then
			joined_at = Dungeons.MASTER_LIST[joined_atID].name
		else
			joined_at = " "
		end
		local tactic = " "
		if Tactic.MASTER_LIST[tacticID] ~= nil then
			tactic = Tactic.MASTER_LIST[tacticID].name
		else
			tactic = " "
		end
		local held_itemTable = Items.GetItem(currAddr + 0x62)
		local held_item = " "
		if held_itemTable ~= nil then
			held_item = held_itemTable.item
		end


		--if (is_team_leader > 0 or is_not_team_member == 0) and (max_hp > 0 and atk > 0 and spatk > 0 and def > 0 and spdef > 0) then
		return {
			valid = 1,
			is_not_team_member = is_not_team_member,
			monster_id = monster_id,
			monsterName = monsterName,
			monsterIcon = monsterIcon,
			level = level,
			joined_atID = joined_atID,
			joined_at = joined_at,
			joined_at_floor = joined_at_floor,
			iq = iq,
			hp = hp,
			max_hp = max_hp,
			max_hp_boost = max_hp_boost,
			exp = exp,
			atk = atk,
			spatk = spatk,
			def = def,
			spdef = spdef,
			tactic = tactic,
			tacticID = tacticID,
			move1 = move1,
			move1ID = move1ID,
			move2 = move2,
			move2ID = move2ID,
			move3 = move3,
			move3ID = move3ID,
			move4 = move4,
			move4ID = move4ID,
			statuses = statuses,
			held_item = held_item,
			is_team_leader = is_team_leader,
			team_index = team_index,
			member_index = 0,
		}
	else
		return {
			valid = 0
		}
	end
end

function DungeonData.GetMonsterStatuses(currAddr)
	local sleep = memory.read_u8(currAddr + 0x14)
	local sleepTurns = memory.read_u8(currAddr + 0x15)

	local burn = memory.read_u8(currAddr + 0x16)
	local burnTurns = memory.read_u8(currAddr + 0x17)

	local freeze = memory.read_u8(currAddr + 0x1B)
	local freezeTurns = memory.read_u8(currAddr + 0x23)


	local cringe = memory.read_u8(currAddr + 0x27)
	local cringeTurns = memory.read_u8(currAddr + 0x28)

	local leechSeed = memory.read_u8(currAddr + 0x37)
	local leechSeedTurns = memory.read_u8(currAddr + 0x40)

	local blinded = memory.read_u8(currAddr + 0x48)
	local blindedTurns = memory.read_u8(currAddr + 0x49)

	local muzzled = memory.read_u8(currAddr + 0x4A)
	local muzzledTurns = memory.read_u8(currAddr + 0x4B)

	local typeChanged = memory.read_u8(currAddr + 0x56)

	local exposed = memory.read_u8(currAddr + 0x55)


	local terrified = memory.read_u8(currAddr + 0x5B)
	local terrifiedTurns = memory.read_u8(currAddr + 0x5C)
end

function DungeonData.GetMonsterStatusIcons(currAddr)
	local byte0     = memory.read_u8(currAddr)
	local bin0      = {}
	bin0            = Program.toBitsLe(byte0)
	local sleepless = 0
	if #bin0 >= 1 and bin0[1] > 0 then
		sleepless = 1
	end
	local burn = 0
	if #bin0 >= 2 and bin0[2] > 0 then
		burn = 1
	end
	local poison = 0
	if #bin0 >= 3 and bin0[3] > 0 then
		poison = 1
	end
	local toxic = 0
	if #bin0 >= 4 and bin0[4] > 0 then
		toxic = 1
	end
	local confused = 0
	if #bin0 >= 5 and bin0[5] > 0 then
		confused = 1
	end
	local cowering = 0
	if #bin0 >= 6 and bin0[6] > 0 then
		cowering = 1
	end
	local taunt = 0
	if #bin0 >= 7 and bin0[7] > 0 then
		taunt = 1
	end
	local encore = 0
	if #bin0 >= 8 and bin0[8] > 0 then
		encore = 1
	end
	--

	local byte1   = memory.read_u8(currAddr + 1)
	local bin1    = {}
	bin1          = Program.toBitsLe(byte1)
	local reflect = 0
	if #bin1 >= 1 and bin1[1] > 0 then
		reflect = 1
	end
	local safeguard = 0
	if #bin1 >= 2 and bin1[2] > 0 then
		safeguard = 1
	end
	local light_screen = 0
	if #bin1 >= 3 and bin1[3] > 0 then
		light_screen = 1
	end
	local protect = 0
	if #bin1 >= 4 and bin1[4] > 0 then
		protect = 1
	end
	local endure = 0
	if #bin1 >= 5 and bin1[5] > 0 then
		endure = 1
	end
	local low_hp = 0
	if #bin1 >= 6 and bin1[6] > 0 then
		low_hp = 1
	end
	local curse = 0
	if #bin1 >= 7 and bin1[7] > 0 then
		curse = 1
	end
	local embargo = 0
	if #bin1 >= 8 and bin1[8] > 0 then
		embargo = 1
	end
	--
	local byte2     = memory.read_u8(currAddr + 2)
	local bin2      = {}
	bin2            = Program.toBitsLe(byte2)
	local sure_shot = 0
	if #bin2 >= 1 and bin2[1] > 0 then
		sure_shot = 1
	end
	local whiffer = 0
	if #bin2 >= 2 and bin2[2] > 0 then
		whiffer = 1
	end
	local set_damage = 0
	if #bin2 >= 3 and bin2[3] > 0 then
		set_damage = 1
	end
	local focus_energy = 0
	if #bin2 >= 4 and bin2[4] > 0 then
		focus_energy = 1
	end
	local blinded = 0
	if #bin2 >= 5 and bin2[5] > 0 then
		blinded = 1
	end
	local cross_eyed = 0
	if #bin2 >= 6 and bin2[6] > 0 then
		cross_eyed = 1
	end
	local eyedrops = 0
	if #bin2 >= 7 and bin2[7] > 0 then
		eyedrops = 1
	end
	local muzzled = 0
	if #bin2 >= 8 and bin2[8] > 0 then
		muzzled = 1
	end
	--
	local byte3  = memory.read_u8(currAddr + 3)
	local bin3   = {}
	bin3         = Program.toBitsLe(byte3)
	local grudge = 0
	if #bin3 >= 1 and bin3[1] > 0 then
		grudge = 1
	end
	local exposed = 0
	if #bin3 >= 2 and bin3[2] > 0 then
		exposed = 1
	end
	local sleep = 0
	if #bin3 >= 3 and bin3[3] > 0 then
		sleep = 1
	end
	local lowered_stat = 0
	if #bin3 >= 4 and bin3[4] > 0 then
		lowered_stat = 1
	end
	local heal_block = 0
	if #bin3 >= 5 and bin3[5] > 0 then
		heal_block = 1
	end
	local miracle_eye = 0
	if #bin3 >= 6 and bin3[6] > 0 then
		miracle_eye = 1
	end
	local red_exclamation_mark = 0
	if #bin3 >= 7 and bin3[7] > 0 then
		red_exclamation_mark = 1
	end
	local magnet_rise = 0
	if #bin3 >= 8 and bin3[8] > 0 then
		magnet_rise = 1
	end
	--
	local byte4  = memory.read_u8(currAddr + 4)
	local bin4   = {}
	bin4         = Program.toBitsLe(byte4)
	local freeze = 0
	if #bin4 >= 1 and bin4[1] > 0 then
		freeze = 1
	end

	return {
		sleepless            = sleepless,
		burn                 = burn, --Red flame
		poison               = poison, --White skull
		toxic                = toxic, --Purple skull
		confused             = confused, --Yellow birds
		cowering             = cowering, --2 green lines in circle (same as whiffer)
		taunt                = taunt,
		encore               = encore,
		--
		reflect              = reflect,
		safeguard            = safeguard,
		light_screen         = light_screen,
		protect              = protect,
		endure               = endure,
		low_hp               = low_hp, --Blue exclamation mark (same as encore)
		curse                = curse, --Red skull
		embargo              = embargo,
		--
		sure_shot            = sure_shot,
		whiffer              = whiffer, --2 green lines in circle (same as cowering)
		set_damage           = set_damage,
		focus_energy         = focus_energy,
		blinded              = blinded, --Blue eye with an X
		cross_eyed           = cross_eyed, --Blue question markBlue question mark
		eyedrops             = eyedrops, --Blue eye blinking yellow with a circular wave
		muzzled              = muzzled, --Blinking red cross
		--
		grudge               = grudge,
		exposed              = exposed,
		sleep                = sleep, --Red Z's
		lowered_stat         = lowered_stat,
		heal_block           = heal_block,
		miracle_eye          = miracle_eye,
		red_exclamation_mark = red_exclamation_mark,
		magnet_rise          = magnet_rise,
		--
		freeze               = freeze, --Ice block
	}
end
