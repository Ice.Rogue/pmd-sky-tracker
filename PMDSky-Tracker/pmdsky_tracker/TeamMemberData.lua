﻿TeamMemberData =
{
	teamTableAddrEU = 0x22AC720,
	teamTableAddrNA = 0x22ABDE0,
	teamTableAddr = 0x22ABDE0,
	teamMemberOffset = 0x936C,
	tableAddr = 0x22ABDE0 + 0x936C,
	teamIdOffset = 0x9877
}
function TeamMemberData.Initialize()
	if Main.NaEu == "EU" then
		TeamMemberData.dungeonTableAddr = TeamMemberData.teamTableAddrEU
	end
	TeamMemberData.tableAddr = TeamMemberData.teamTableAddr + TeamMemberData.teamMemberOffset
end

function TeamMemberData.GetTeamData()
	TeamMemberData.Initialize()
	local team_id = memory.read_u8(TeamMemberData.teamTableAddr + TeamMemberData.teamIdOffset)
	--console.log("Team_id: " .. team_id)
	local tableSize = 104
	local startindx = team_id * 4 * tableSize
	local endindx = (team_id + 1) * 4 * tableSize
	local tempActiveTeam = {}
	for i = startindx, endindx, 104 do --for i = 0, 37636, 104 do
		local currAddr = TeamMemberData.tableAddr + i
		local newmon = TeamMemberData.GetMonsterData(currAddr)
		if newmon.valid > 0 then
			local monID = Program.toID(newmon.monster_id, newmon.joined_atID, newmon.joined_at_floor)
			if Main.fullTeam[monID] then
				if Main.fullTeam[monID].exp <= newmon.exp or Main.fullTeam[monID].member_index ~= newmon.member_index then
					Main.fullTeam[monID].level = newmon.level
					Main.fullTeam[monID].max_hp = newmon.max_hp
					Main.fullTeam[monID].iq = newmon.iq
					Main.fullTeam[monID].exp = newmon.exp
					Main.fullTeam[monID].atk = newmon.atk
					Main.fullTeam[monID].spatk = newmon.spatk
					Main.fullTeam[monID].def = newmon.def
					Main.fullTeam[monID].spdef = newmon.spdef
					Main.fullTeam[monID].tactic = newmon.tactic
					Main.fullTeam[monID].tacticID = newmon.tacticID
					Main.fullTeam[monID].move1 = newmon.move1
					Main.fullTeam[monID].move1ID = newmon.move1ID
					Main.fullTeam[monID].move2 = newmon.move2
					Main.fullTeam[monID].move2ID = newmon.move2ID
					Main.fullTeam[monID].move3 = newmon.move3
					Main.fullTeam[monID].move3ID = newmon.move3ID
					Main.fullTeam[monID].move4 = newmon.move4
					Main.fullTeam[monID].move4ID = newmon.move4ID
					Main.fullTeam[monID].member_index = newmon.member_index
					Main.fullTeam[monID].team_index = newmon.team_index
					Main.fullTeam[monID].is_team_leader = newmon.is_team_leader
					Main.fullTeam[monID].held_item = newmon.held_item
				end
			end
			Main.activeTeam[monID] = newmon
			tempActiveTeam[monID] = newmon
		end
	end
	--console.log(tempActiveTeam)
	--if #tempActiveTeam > 0 then
	--	Main.activeTeam = {}
	for k, v in pairs(Main.activeTeam) do
		if tempActiveTeam[k] == nil then
			Main.activeTeam[k] = nil
		end
	end
end

function TeamMemberData.GetMonsterData(currAddr)
	local valid = memory.read_u8(currAddr)
	if (valid > 1) then
		local is_team_leader = memory.read_u8(currAddr + 0x1)
		local level = memory.read_u8(currAddr + 0x2)
		local joined_atID = memory.read_u8(currAddr + 0x3)
		local joined_at_floor = memory.read_u8(currAddr + 0x4)
		local iq = memory.read_u16_le(currAddr + 0x6)
		local member_index = memory.read_u16_le(currAddr + 0x8)
		local team_index = memory.read_u16_le(currAddr + 0xA)
		local monster_id = memory.read_u16_le(currAddr + 0xC)
		local hp = memory.read_u16_le(currAddr + 0xE)
		local max_hp = memory.read_u16_le(currAddr + 0x10)
		local atk = memory.read_u8(currAddr + 0x12)
		local spatk = memory.read_u8(currAddr + 0x13)
		local def = memory.read_u8(currAddr + 0x14)
		local spdef = memory.read_u8(currAddr + 0x15)
		local exp = memory.read_s32_le(currAddr + 0x18)

		--struct item held_item;         // 0x3E
		local held_itemTable = Items.GetItem(currAddr + 0x3E)
		local held_item = " "
		if held_itemTable ~= nil then
			held_item = held_itemTable.item
		end

		local moves = Moves.GetDungeonMoves(currAddr + 0x1C)
		local move1 = moves.move1
		local move1ID = moves.move1ID
		local move2 = moves.move2
		local move2ID = moves.move2ID
		local move3 = moves.move3
		local move3ID = moves.move3ID
		local move4 = moves.move4
		local move4ID = moves.move4ID

		local tacticID = memory.read_u8(currAddr + 0x58)
		local name = Program.ReadName(currAddr + 0x5E) .. " "

		if monster_id > 600 then monster_id = monster_id - 600 end
		local monsterName = " "
		if Monster_ID.MonsterID_MASTER_LIST[monster_id] ~= nil then
			monsterName = Monster_ID.MonsterID_MASTER_LIST[monster_id].name
		end
		local monsterIcon = Monster_ID.MonsterID_IMG_LIST[string.lower(monsterName)]

		if Dungeons.MASTER_LIST[joined_atID] ~= nil then
			joined_at = Dungeons.MASTER_LIST[joined_atID].name
		else
			joined_at = " "
		end
		local tactic = " "
		if Tactic.MASTER_LIST[tacticID] ~= nil then
			tactic = Tactic.MASTER_LIST[tacticID].name
		else
			tactic = " "
		end



		if exp < 0 or joined_at == "Dummy" or monster_id <= 0 or joined_atID <= 0 or monster_id > 600 or move1ID > 600 or move2ID > 600 or move3ID > 600 or move4ID > 600 then
			return {
				valid = 0
			}
		end


		--if (is_team_leader > 0 or is_not_team_member == 0) and (max_hp > 0 and atk > 0 and spatk > 0 and def > 0 and spdef > 0) then
		return {
			valid = valid,

			monster_id = monster_id,
			monsterName = monsterName,
			monsterIcon = monsterIcon,
			level = level,
			joined_atID = joined_atID,
			joined_at = joined_at,
			joined_at_floor = joined_at_floor,
			iq = iq,
			hp = hp,
			max_hp = max_hp,
			name = name,
			exp = exp,
			atk = atk,
			spatk = spatk,
			def = def,
			spdef = spdef,
			tactic = tactic,
			tacticID = tacticID,
			move1 = move1,
			move1ID = move1ID,
			move2 = move2,
			move2ID = move2ID,
			move3 = move3,
			move3ID = move3ID,
			move4 = move4,
			move4ID = move4ID,
			held_item = held_item,
			is_team_leader = is_team_leader,
			team_index = team_index,
			member_index = member_index,
		}
	else
		return {
			valid = 0
		}
	end
end
