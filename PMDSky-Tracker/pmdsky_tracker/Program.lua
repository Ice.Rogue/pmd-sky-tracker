﻿Program =
{
	iconBoxSpacing = 2,
}

--Overlay #29:
--
--    RAM Load address: 0x022DC240
--    Length in RAM: 488,992 bytes
--    Description: Possibly having to do with running the in-dungeon game. Spotted strings for the in-dungeon menu.
--




function Program.GetData()
	--local shift = 0xFFFFFF
	--local carriedAddr = 0x22A4BB8
	--local bankAddr = 0x22A4BC4
	--local teamNameAddr = 0x22AB918
	--local teamTableAddr = 0x22ABDE0
	--local dungeonTableAddr = 0x21B9D34
	--struct monster monsters[20]; // 0x7F4: Info for all the monsters currently in the dungeon
	--local monstersDungeonOffset = 0x7F4
	--local teamMemberStructOffset = 0x936C
	-- Pokemon Mystery Dungeon - Explorers of Sky (Europe) (En,Fr,De,Es,It)
	--if Main.NaEu == "EU" then
	--carriedAddr = 0x22A54F8
	--bankAddr = 0x22A5504
	--teamNameAddr = 0x22AC258
	--teamTableAddr = 0x22AC720
	--dungeonTableAddr = 0x21BA674
	--end

	local teamMembers = 0
	local backupTeam = Main.team

	Team.PopulateTeamList()

	local tBelly = {}
	tBelly = DungeonData.GetBelly()
	Main.belly = tBelly.belly
	Main.max_belly = tBelly.max_belly

	local overlayCheck = Overlays.DungeonOverlayLoaded()
	--console.log(dungeon)
	--if dungeon.dungeonName ~= "Dummy" and dungeon.dungeon_id > 0 and dungeon.currentFloor < 100 and dungeon.currentFloor > 0 then
	if overlayCheck > 0 then
		DungeonData.GetTeamData()
	else
		TeamMemberData.GetTeamData()
	end
end

function Program.toID(monster_id, joined_at, joined_at_floor)
	if not (monster_id and joined_at and joined_at_floor) then return -1 end
	return (monster_id << 0x16) + (joined_at << 0x8) + (joined_at_floor)
end

function Program.GetPosionOfImage(i)
	local x = Main.posXBase
	local y = Main.posYBase - Main.spaceY
	for j = 1, i do
		x = x + Main.spaceX
		if (j - 1) % Main.numPerLine == 0 then
			x = Main.posXBase + Main.spaceX
			y = y + Main.spaceY
		end
	end
	return { Xpos = x, Ypos = y }
end

function Program.ReadName(currAddr)
	local name = ""
	local nameBytes = memory.read_bytes_as_array(currAddr, 10)
	for i, v in ipairs(nameBytes) do
		-- print(teamNameBytes[i])
		if nameBytes[i] > 0 then name = name .. utf8.char(nameBytes[i]) end
	end
	return name
end

function Program.CheckForHover()
	-- print("Main.CheckForHover() " )
	local mouse = Input.getMouse()
	-- Input.updateMouse()
	local mousePosition = Input.getMousePosition()
	if Main.team ~= nil then
		for k, v in pairs(Main.fullTeam) do
			if v ~= nil then
				--local position = Program.GetPosionOfImage(i)
				local inRange = Program.mouseInRange(mousePosition.x,
					mousePosition.y, v.posX, v.posY, 32, 32)
				if inRange then
					Drawing.DrawStats(v)
				end
			end
		end
		for k, v in pairs(Main.activeTeam) do
			if v ~= nil then
				--local position = Program.GetPosionOfImage(i)
				local inRange = Program.mouseInRange(mousePosition.x,
					mousePosition.y, v.posX, v.posY, 32, 32)
				if inRange then
					Drawing.DrawStats(v)
				end
			end
		end
	end
	local inRangeLog = Program.mouseInRange(mousePosition.x,
		mousePosition.y, 335, 20, 150, 20)
	if inRangeLog then
		AdventureLog.DrawAdventureLog()
	end
end

function Program.mouseInRange(mouseX, mouseY, controlX, controlY, width, height)
	if mouseX >= controlX and mouseX <= controlX + width then
		if mouseY >= controlY and mouseY <= controlY + height then
			return true
		end
	end
	return false
end

function Program.toBitsLe(num)
	-- returns a table of bits, least significant first.
	local t = {} -- will contain the bits
	while num > 0 do
		rest = math.fmod(num, 2)
		t[#t + 1] = rest
		num = (num - rest) / 2
	end
	return t
end

function Program.BitFieldMap(n)
	local bin  = {}
	bin        = Program.toBitsLe(n)
	local bin0 = 0
	if #bin >= 1 and bin[1] > 0 then
		bin0 = 1
	end
	local bin1 = 0
	if #bin >= 2 and bin[2] > 0 then
		bin1 = 1
	end
	local bin2 = 0
	if #bin >= 3 and bin[3] > 0 then
		bin2 = 1
	end
	local bin3 = 0
	if #bin >= 4 and bin[4] > 0 then
		bin3 = 1
	end
	local bin4 = 0
	if #bin >= 5 and bin[5] > 0 then
		bin4 = 1
	end
	local bin5 = 0
	if #bin >= 6 and bin[6] > 0 then
		bin5 = 1
	end
	local bin6 = 0
	if #bin >= 7 and bin[7] > 0 then
		bin6 = 1
	end
	local bin7 = 0
	if #bin >= 8 and bin[8] > 0 then
		bin7 = 1
	end
	return {
		bin0 = bin0,
		bin1 = bin1,
		bin2 = bin2,
		bin3 = bin3,
		bin4 = bin4,
		bin5 = bin5,
		bin6 = bin6,
		bin7 = bin7,
	}
end

function Program.toBitsBe(num, bits)
	-- returns a table of bits, most significant first.
	bits = bits or math.max(1, select(2, math.frexp(num)))
	local t = {} -- will contain the bits
	for b = bits, 1, -1 do
		t[b] = math.fmod(num, 2)
		num = math.floor((num - t[b]) / 2)
	end
	return t
end
