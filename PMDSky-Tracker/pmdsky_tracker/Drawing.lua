﻿Drawing = {
	carriedAddr = 0x22A4BB8,
	carriedAddrNA = 0x22A4BB8,
	carriedAddrEU = 0x22A54F8,
	bankAddr = 0x22A4BC4,
	bankAddrNA = 0x22A4BC4,
	bankAddrEU = 0x22A5504,
	teamNameAddr = 0x22AB918,
	teamNameAddrNA = 0x22AB918,
	teamNameAddrEU = 0x22AC258,
	teamTableAddr = 0x22ABDE0,
	teamTableAddrNA = 0x22ABDE0,
	teamTableAddrEU = 0x22AC720,
	dungeonTableAddr = 0x21B9D34,
	dungeonTableAddrNA = 0x21B9D34,
	dungeonTableAddrEU = 0x21BA674,
	monstersDungeonOffset = 0x7F4,
	teamMemberStructOffset = 0x936C,

}

function Drawing.Initialize()
	if Main.NaEu == "EU" then
		Drawing.carriedAddr = Drawing.carriedAddrEU
		Drawing.bankAddr = Drawing.bankAddrEU
		Drawing.teamNameAddr = Drawing.teamNameAddrEU
		Drawing.teamTableAddr = Drawing.teamTableAddrEU
		Drawing.dungeonTableAddr = Drawing.dungeonTableAddrEU
	end
end

function Drawing.DrawHeader()
	Drawing.Initialize()
	local advLog = AdventureLog.GetAdventureLog()
	Drawing.drawLargeText(335, 6, "PMD: EoS Tracker", Constants.Color.IceBlue,
		Constants.Color.Black, "bold")
	local teamName = Program.ReadName(Drawing.teamNameAddr) --"WWWWWWWWWWW" --
	Drawing.drawSplitText(270, 18, "Team: ", teamName, Constants.Color.LtBlue, Constants.Color.White,
		Constants.Color.DkGrey)

	local carriedMoney = memory.read_u32_le(Drawing.carriedAddr)

	Drawing.drawSplitText(420, 18, "Money: ", carriedMoney, Constants.Color.LtBlue, Constants.Color.White,
		Constants.Color.DkGrey)


	local bellyColor = Constants.Color.Green
	if Main.belly <= 20 and Main.max_belly > 0 then
		bellyColor = Constants.Color.Red
	elseif Main.belly <= 50 and Main.max_belly > 0 then
		bellyColor = Constants.Color.Yellow
	end

	Drawing.drawSplitText(265, 32, "Belly: ", Main.belly .. "/" .. Main.max_belly, Constants.Color.LtBlue,
		bellyColor,
		Constants.Color.DkGrey)
	Drawing.drawSplitText(335, 32, "Faints: ", advLog.faints, Constants.Color.LtBlue,
		Constants.Color.White, Constants.Color.DkGrey)
	Drawing.drawSplitText(385, 32, "Joins: ", advLog.pokemonJoinedCounter, Constants.Color.LtBlue,
		Constants.Color.White, Constants.Color.DkGrey)
	Drawing.drawSplitText(430, 32, "Recycle: ", advLog.recycled, Constants.Color.LtBlue,
		Constants.Color.White, Constants.Color.DkGrey)
end

function Drawing.DrawTeam()
	Drawing.Initialize()

	--console.log(Main.activeTeam)
	--console.log(Main.fullTeam)


	for k, v in pairs(Main.activeTeam) do
		if v ~= nil then
			local position = Program.GetPosionOfImage(v.team_index + 1)
			-- Monster Icon
			--console.log(v)
			--console.log(v.team_index)
			Main.activeTeam[k].posX = position.Xpos
			Main.activeTeam[k].posY = position.Ypos

			if (v.monsterIcon == nil) then
				Program.drawSmallText(position.Xpos, position.Ypos,
					v.monsterName,
					Constants.Color.White, Constants.Color.DkGrey)
			else
				local imagepath = Main.DataFolder .. "/mysteryDungeonSet/" ..
					v.monsterIcon.name
				local status = ""
				Drawing.drawIcon(imagepath, position.Xpos, position.Ypos, v.statuses, v.hp,
					v.max_hp)
			end
		end
	end
	local imageCount = 5
	for k, v in pairs(Main.fullTeam) do
		--console.log(v)
		if Main.activeTeam[k] == nil then
			local position = Program.GetPosionOfImage(imageCount)
			Main.fullTeam[k].posX = position.Xpos
			Main.fullTeam[k].posY = position.Ypos
			imageCount = imageCount + 1
			-- Monster Icon
			if (v.monsterIcon == nil) then
				Drawing.drawSmallText(position.Xpos, position.Ypos,
					v.monsterName,
					Constants.Color.White, Constants.Color.DkGrey)
			else
				local imagepath = Main.DataFolder .. "/mysteryDungeonSet/" ..
					v.monsterIcon.name
				local status = ""
				Drawing.drawIcon(imagepath, position.Xpos, position.Ypos, v.statuses, v.hp,
					v.max_hp)
			end
		end
	end
end

function Drawing.stringPixelLength(text)
	local totalLength = 0
	for i = 1, #text do
		local char = text:sub(i, i)
		if Constants.LETTER_PIXEL_LENGTHS[char] then
			totalLength = totalLength + Constants.LETTER_PIXEL_LENGTHS[char]
		else
			totalLength = totalLength + 1
		end
	end
	totalLength = totalLength + #text --space in between each character
	return totalLength
end

function Drawing.drawSplitText(x, y, t1, t2, color1, color2, shadowcolor, style)
	local spacing = 1.3 * Drawing.stringPixelLength(t1)
	Drawing.drawText(x, y, t1, color1, shadowcolor, style)
	Drawing.drawText(x + spacing, y, t2, color2, shadowcolor, style)
end

function Drawing.drawText(x, y, text, color, shadowcolor, style)
	gui.drawText(x + 1, y + 1, text, shadowcolor, nil, Constants.Font.SIZE, Constants.Font.FAMILY, style)
	gui.drawText(x, y, text, color, nil, Constants.Font.SIZE, Constants.Font.FAMILY, style)
end

function Drawing.drawSplitStatText(x, y, t1, t2, color1, color2, shadowcolor, style)
	local spacing = 1.2 * Drawing.stringPixelLength(t1)
	Drawing.drawStatText(x, y, t1, color1, shadowcolor, style)
	Drawing.drawStatText(x + spacing, y, t2, color2, shadowcolor, style)
end

function Drawing.drawSplitLargeText(x, y, t1, t2, color1, color2, shadowcolor, style)
	local spacing = 1.3 * Drawing.stringPixelLength(t1)
	Drawing.drawStatText(x, y, t1, color1, shadowcolor, style)
	Drawing.drawStatText(x + spacing, y, t2, color2, shadowcolor, style)
end

function Drawing.drawSplitSmallText(x, y, t1, t2, color1, color2, shadowcolor, style)
	local spacing = 1.5 * Drawing.stringPixelLength(t1)
	Drawing.drawLargeText(x, y, t1, color1, shadowcolor, style)
	Drawing.drawLargeText(x + spacing, y, t2, color2, shadowcolor, style)
end

function Drawing.drawStatText(x, y, text, color, shadowcolor, style)
	gui.drawText(x + 1, y + 1, text, shadowcolor, nil, Constants.Font.SizeSats, Constants.Font.FAMILY, style)
	gui.drawText(x, y, text, color, nil, Constants.Font.SizeSats, Constants.Font.FAMILY, style)
end

function Drawing.drawLargeText(x, y, text, color, shadowcolor, style)
	gui.drawText(x + 1, y + 1, text, shadowcolor, nil, Constants.Font.SizeLarge, Constants.Font.FAMILY, style)
	gui.drawText(x, y, text, color, nil, Constants.Font.SizeLarge, Constants.Font.FAMILY, style)
end

function Drawing.drawSmallText(x, y, text, color, shadowcolor, style)
	gui.drawText(x + 1, y + 1, text, shadowcolor, nil, Constants.Font.SizeSmall, Constants.Font.FAMILY, style)
	gui.drawText(x, y, text, color, nil, Constants.Font.SizeSmall, Constants.Font.FAMILY, style)
end

function Drawing.drawIcon(imagepath, Xpos, Ypos, statuses, hp, max_hp)
	local boxSize = 32 + 2 * Program.iconBoxSpacing
	local borderColor = Constants.Color.Black
	local backColor = Constants.Color.NeroGrey

	if statuses ~= nil and #statuses > 0 then
		if statuses.toxic ~= nil and statuses.toxic > 0 then
			backColor = Constants.Color.Purple
		elseif statuses.poison ~= nil and statuses.poison > 0 then
			backColor = Constants.Color.White
		elseif statuses.burn ~= nil and statuses.burn > 0 then
			backColor = Constants.Color.Red
		end
	end

	if hp ~= nil and max_hp ~= nil then
		local percentHealth = 100 * hp / max_hp
		if percentHealth == 0 then
			borderColor = Constants.Color.Black
		elseif percentHealth > 75 then
			borderColor = Constants.Color.Green
		elseif percentHealth > 50 then
			borderColor = Constants.Color.Yellow
		elseif percentHealth > 25 then
			borderColor = Constants.Color.Orange
		else
			borderColor = Constants.Color.Red
		end
	end

	-- void gui.drawRectangle(int x, int y, int width, int height, [luacolor line = nil], [luacolor background = nil], [string surfacename = nil])
	gui.drawRectangle(Xpos - Program.iconBoxSpacing, Ypos - Program.iconBoxSpacing, boxSize, boxSize, borderColor,
		backColor)
	gui.drawImage(imagepath, Xpos, Ypos)
end

function Drawing.DrawStats(m)
	local rsp = 15
	local csp3 = 75
	local csp2 = 110
	local xb = 270
	local yb = 250

	-- void gui.drawRectangle(int x, int y, int width, int height, [luacolor line = nil], [luacolor background = nil], [string surfacename = nil])
	gui.drawRectangle(265, 245, 230, 110, Constants.Color.IceBlue, Constants.Color.DkNeroGrey)

	Drawing.drawSplitStatText(xb, yb, "Name: ", m.name, Constants.Color.LtBlue,
		Constants.Color.White, Constants.Color.DkGrey)

	Drawing.drawSplitStatText(xb + csp2, yb, "Joined: ", m.joined_at, Constants.Color.LtBlue,
		Constants.Color.White, Constants.Color.DkGrey)
	---
	---

	Drawing.drawSplitStatText(xb, yb + rsp, "Lvl: ", m.level, Constants.Color.LtBlue,
		Constants.Color.White, Constants.Color.DkGrey)

	Drawing.drawSplitStatText(xb + csp3, yb + rsp, "Tactic: ", m.tactic, Constants.Color.LtBlue,
		Constants.Color.White, Constants.Color.DkGrey)

	---
	---
	Drawing.drawSplitStatText(xb, yb + rsp * 2, "Max HP: ", m.max_hp, Constants.Color.LtBlue,
		Constants.Color.White, Constants.Color.DkGrey)

	Drawing.drawSplitStatText(xb + csp3, yb + rsp * 2, "Atk: ", m.atk, Constants.Color.LtBlue,
		Constants.Color.White, Constants.Color.DkGrey)

	Drawing.drawSplitStatText(xb + csp3 * 2, yb + rsp * 2, "SpAtk: ", m.spatk, Constants.Color.LtBlue,
		Constants.Color.White, Constants.Color.DkGrey)
	---
	---
	Drawing.drawSplitStatText(xb, yb + rsp * 3, "IQ: ", m.iq, Constants.Color.LtBlue,
		Constants.Color.White, Constants.Color.DkGrey)

	Drawing.drawSplitStatText(xb + csp3, yb + rsp * 3, "Def: ", m.def, Constants.Color.LtBlue,
		Constants.Color.White, Constants.Color.DkGrey)

	Drawing.drawSplitStatText(xb + csp3 * 2, yb + rsp * 3, "SpDef: ", m.spdef, Constants.Color.LtBlue,
		Constants.Color.White, Constants.Color.DkGrey)
	---
	---
	Drawing.drawSplitStatText(xb, yb + rsp * 4, "Move 1: ", m.move1, Constants.Color.LtBlue,
		Constants.Color.White, Constants.Color.DkGrey)

	Drawing.drawSplitStatText(xb + csp2, yb + rsp * 4, "Move 2: ", m.move2, Constants.Color.LtBlue,
		Constants.Color.White, Constants.Color.DkGrey)
	---
	---
	Drawing.drawSplitStatText(xb, yb + rsp * 5, "Move 3: ", m.move3, Constants.Color.LtBlue,
		Constants.Color.White, Constants.Color.DkGrey)

	Drawing.drawSplitStatText(xb + csp2, yb + rsp * 5, "Move 4: ", m.move4, Constants.Color.LtBlue,
		Constants.Color.White, Constants.Color.DkGrey)
	---
	---
	Drawing.drawSplitStatText(xb, yb + rsp * 6, "Held Item: ", m.held_item, Constants.Color.LtBlue,
		Constants.Color.White, Constants.Color.DkGrey)

	-- Program.drawText(xb + csp, yb + rsp * 3, "Move 2: " .. m.move2, Constants.Color.White, "#FF010101")

	-- Program.drawText(xb + csp * 2, yb + rsp * 3, "Move 3: " .. m.move3, Constants.Color.White, "#FF010101")
	---
	---
end
