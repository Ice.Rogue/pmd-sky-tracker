﻿Tactic = {}

Tactic.MASTER_LIST = {

	[0] = { name = "Lets Go Together" },
	[1] = { name = "You Go The Other Way" },
	[2] = { name = "Go After Foes" },
	[3] = { name = "Avoid The First Hit" },
	[4] = { name = "All For One" },
	[5] = { name = "Group Safety" },
	[6] = { name = "Avoid Trouble" },
	[7] = { name = "Be Patient" },
	[8] = { name = "Keep Your Distance" },
	[9] = { name = "Wait There" },
	[10] = { name = "Get Away From Here" },
	[11] = { name = "None" }, 
		-- used as an empty placeholder in menus (and maybe other things).
	
}