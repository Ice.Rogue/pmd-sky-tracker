﻿Overlays =
{
	ddrGroupZeroEU = 0x20AFAD0,
	ddrGroupZeroNA = 0x20AF230,
	addrGroupZero = 0x20AF230,
	ddrGroupOneEU = 0x20AFAD4,
	ddrGroupOneNA = 0x20AF234,
	addrGroupOne = 0x20AF234,
	ddrGroupTwoEU = 0x20AFAD8,
	ddrGroupTwoNA = 0x20AF238,
	addrGroupTwo = 0x20AF238,
}

function Overlays.Initialize()
	if Main.NaEu == "EU" then
		Overlays.addrGroupZero = Overlays.ddrGroupZeroEU
		Overlays.addrGroupOne = Overlays.ddrGroupOneEU
		Overlays.addrGroupTwo = Overlays.ddrGroupTwoEU
	end
end

function Overlays.GetLoadedOverlays()
	Overlays.Initialize()
	local groupZeroId = memory.read_u32_le(Overlays.addrGroupZero)
	local groupOneId = memory.read_u32_le(Overlays.addrGroupOne)
	local groupTwoId = memory.read_u32_le(Overlays.addrGroupTwo)
	local groupZero = " "
	if Overlays.OverlayGroupZeroIDs[groupZeroId] then
		groupZero = Overlays.OverlayGroupZeroIDs[groupZeroId].name
	else
		groupZero = "ID: " .. groupZeroId
	end
	local groupOne = " "
	if Overlays.OverlayGroupOneIDs[groupOneId] then
		groupOne = Overlays.OverlayGroupOneIDs[groupOneId].name
	else
		groupOne = "ID: " .. groupOneId
	end
	local groupTwo = " "
	if Overlays.OverlayGroupTwoIDs[groupTwoId] then
		groupTwo = Overlays.OverlayGroupTwoIDs[groupTwoId].name
	else
		groupTwo = "ID: " .. groupTwoId
	end

	return {
		groupZeroId = groupZeroId,
		groupOneId = groupOneId,
		groupTwoId = groupTwoId,
		groupZero = groupZero,
		groupOne = groupOne,
		groupTwo = groupTwo,
	}
end

function Overlays.DungeonOverlayLoaded()
	local loaded = Overlays.GetLoadedOverlays()
	local dungeonOverlays = 0
	if loaded.groupZero == "Overlay 31" then
		dungeonOverlays = dungeonOverlays + 1
	end
	if loaded.groupOne == "Overlay 29" then
		dungeonOverlays = dungeonOverlays + 1
	end
	return dungeonOverlays
end

Overlays.OverlayGroupZeroIDs =
{
	[0x0] = { name = "None" },
	[0x06] = { name = "Overlay 3" },
	[0x07] = { name = "Overlay 6" },
	[0x08] = { name = "Overlay 4" },
	[0x09] = { name = "Overlay 5" },
	[0x0A] = { name = "Overlay 7" },
	[0x0B] = { name = "Overlay 8" },
	[0x0C] = { name = "Overlay 9" },
	[0x10] = { name = "Overlay 12" },
	[0x11] = { name = "Overlay 13" },
	[0x12] = { name = "Overlay 14" },
	[0x13] = { name = "Overlay 15" },
	[0x14] = { name = "Overlay 16" },
	[0x15] = { name = "Overlay 17" },
	[0x16] = { name = "Overlay 18" },
	[0x17] = { name = "Overlay 19" },
	[0x18] = { name = "Overlay 20" },
	[0x19] = { name = "Overlay 21" },
	[0x1A] = { name = "Overlay 22" },
	[0x1B] = { name = "Overlay 23" },
	[0x1C] = { name = "Overlay 24" },
	[0x1D] = { name = "Overlay 25" },
	[0x1E] = { name = "Overlay 26" },
	[0x1F] = { name = "Overlay 27" },
	[0x20] = { name = "Overlay 28" },
	[0x21] = { name = "Overlay 30" },
	[0x22] = { name = "Overlay 31" },
	[0x23] = { name = "Overlay 32" },

}
Overlays.OverlayGroupOneIDs =
{
	[0x0] = { name = "None" },
	[0x4] = { name = "Overlay 1" },
	[0x5] = { name = "Overlay 2" },
	[0xD] = { name = "Overlay 11" },
	[0xE] = { name = "Overlay 29" },
	[0xF] = { name = "Overlay 34" },
}
Overlays.OverlayGroupTwoIDs =
{
	[0x0] = { name = "None" },
	[0x1] = { name = "Overlay 0" },
	[0x2] = { name = "Overlay 10" },
	[0x3] = { name = "Overlay 35" },
}
